# ReTwit - Responsive Twitter Widget

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Clone this repository

If Composer is installed globally, run

```bash
composer install
```


To run CakePHP dev server execute

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765/widgets/twitter/nasa` to see the widget.
