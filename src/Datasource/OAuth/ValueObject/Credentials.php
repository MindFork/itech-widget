<?php

namespace App\Datasource\OAuth\ValueObject;

use App\Datasource\OAuth\Exception\InvalidCredentialsException;

final class Credentials
{
    /**
     * API key.
     *
     * @var string
     */
    private $consumerKey;

    /**
     * API secret.
     *
     * @var string
     */
    private $consumerSecret;

    /**
     * Access token.
     *
     * @var string
     */
    private $accessToken;

    /**
     * Access token secret.
     *
     * @var string
     */
    private $accessTokenSecret;

    /**
     * Credentials constructor.
     *
     * @param string $consumerKey
     * @param string $consumerSecret
     * @param string $accessToken
     * @param string $accessTokenSecret
     * @throws InvalidCredentialsException
     */
    public function __construct($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret)
    {
        if (empty($consumerKey) || empty($consumerSecret) || empty($accessToken) || empty($accessTokenSecret)) {
            throw new InvalidCredentialsException('All parameters are requiered');
        }
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->accessToken = $accessToken;
        $this->accessTokenSecret = $accessTokenSecret;
    }

    /**
     * @return string
     */
    public function getConsumerKey()
    {
        return $this->consumerKey;
    }

    /**
     * @return string
     */
    public function getConsumerSecret()
    {
        return $this->consumerSecret;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    public function getAccessTokenSecret()
    {
        return $this->accessTokenSecret;
    }


}
