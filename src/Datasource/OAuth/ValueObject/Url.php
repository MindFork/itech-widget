<?php

namespace App\Datasource\OAuth\ValueObject;

use App\Datasource\OAuth\Exception\InvalidUrlException;

final class Url
{
    /**
     * Domain part of the URL.
     *
     * @var string
     */
    private $address;

    /**
     * Path part of the URL.
     *
     * @var string
     */
    private $path;

    /**
     * Should SSL be used (i.e. HTTP vs HTTPS).
     *
     * @var bool
     */
    private $ssl;

    /**
     * Url constructor.
     *
     * @param string $address
     * @param string $path
     * @param bool   $ssl
     * @throws InvalidUrlException
     */
    public function __construct($address, $path = '', $ssl = true)
    {
        if (empty($address)) {
            throw new InvalidUrlException('Address cannot be empty!');
        }
        $this->address = $address;
        $this->path = $path;
        $this->ssl = $ssl;
    }

    /**
     * Get full URL containing protocol, domain and path.
     *
     * @return string
     */
    public function getFullUrl()
    {
        return ($this->ssl ? 'https://' : 'http://') . $this->address . $this->path;
    }

    /**
     * Domain part of the URL.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Path part of the URL.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Should SSL be used (i.e. HTTP vs HTTPS).
     *
     * @return bool
     */
    public function isSsl()
    {
        return $this->ssl;
    }


}
