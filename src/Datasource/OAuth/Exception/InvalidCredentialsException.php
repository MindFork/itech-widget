<?php

namespace App\Datasource\OAuth\Exception;

class InvalidCredentialsException extends \Exception {}
