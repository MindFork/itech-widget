<?php

namespace App\Datasource\OAuth;

use App\Datasource\OAuth\Exception\InvalidUrlException;
use App\Datasource\OAuth\ValueObject\Credentials;
use App\Datasource\OAuth\ValueObject\Url;

class Request
{
    /**
     * URL for oAuth Request.
     *
     * @var Url
     */
    private $url;

    /**
     * Credentials for OAuth authenticated API calls.
     *
     * @var Credentials
     */
    private $credentials;

    /**
     * HTTP method for the Request.
     *
     * @var string
     */
    private $httpMethod;

    /**
     * Array of parameters for building Authorization header.
     *
     * @var array
     */
    private $oAuthParams;

    /**
     * Request constructor.
     * @param Credentials|null $credentials
     * @param Url $url
     * @param string $method
     */
    public function __construct(Credentials $credentials, Url $url = null, $method = 'GET')
    {
        $this->credentials = $credentials;
        $this->url = $url;
        $this->httpMethod = $method;
        $this->inflateAuthParams();
    }

    /**
     * @param array $params
     * @return mixed
     * @throws InvalidUrlException
     */
    public function execute($params = [])
    {
        $this->validate();
        $this->generateOAuthSignature($params);
        $response = json_decode($this->executeRequest($this->url->getFullUrl() . '?' . http_build_query($params)));
        return $response;
    }

    /**
     * Sets array of parameters used for generating Authorization header.
     */
    private function inflateAuthParams()
    {
        $this->oAuthParams = [
            'oauth_consumer_key' => $this->credentials->getConsumerKey(),
            'oauth_token' => $this->credentials->getAccessToken(),
            'oauth_nonce' => (string)mt_rand(),
            'oauth_timestamp' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_version' => '1.0'
        ];
    }

    /**
     * Sets signature param for generating Authorization header.
     *
     * @param array $params
     */
    private function generateOAuthSignature($params)
    {
        $this->oAuthParams['oauth_signature'] = $this->getOAuthSignature(
            $this->getHttpMethod(),
            $this->getUrl()->getFullUrl(),
            $this->getQueryString($params)
        );
    }

    /**
     * Validation rules for executing request.
     *
     * @throws InvalidUrlException
     */
    private function validate()
    {
        if ($this->getUrl() === null) {
            throw new InvalidUrlException('Url is missing!');
        }
    }

    /**
     * Execute actual CURL call.
     *
     * @param $url
     * @return mixed
     */
    private function executeRequest($url)
    {
        $handler = curl_init();
        curl_setopt_array($handler, [
            CURLOPT_HTTPHEADER => [
                'Authorization: OAuth ' . urldecode(http_build_query($this->getOAuthParams(), '', ', '))
            ],
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        ]);
        $json = curl_exec($handler);
        curl_close($handler);
        return $json;
    }

    /**
     * Generate OAuth Signature string.
     *
     * @param string $method HTTP method
     * @param string $url Full URL including protocol, domain and path
     * @param string $queryString Query string to be added to URL
     * @return string
     */
    private function getOAuthSignature($method, $url, $queryString)
    {
        $baseString = $method . "&" . rawurlencode($url) . "&" . rawurlencode($queryString);
        $key = rawurlencode($this->credentials->getConsumerSecret()) . "&" . rawurlencode($this->credentials->getAccessTokenSecret());
        return rawurlencode(base64_encode(hash_hmac('sha1', $baseString, $key, true)));
    }

    /**
     * Fetch sorted array of parameters for generating Query String.
     *
     * @param array $query Parameters for API call - excluding oAuth params
     * @return array
     */
    private function getQueryStringParams($query)
    {
        $queryStringParams = array_merge($this->getOAuthParams(), $query);
        asort($queryStringParams);
        ksort($queryStringParams);
        $queryStringParams = array_map("rawurlencode", $queryStringParams);
        return $queryStringParams;
    }

    /**
     * Get Query String based on array of parameters.
     *
     * @param array $query
     * @return array
     */
    private function getQueryString($query)
    {
        return urldecode(http_build_query($this->getQueryStringParams($query), '', '&'));
    }

    /**
     * Returns current OAuth params.
     *
     * @return array
     */
    private function getOAuthParams()
    {
        return $this->oAuthParams;
    }

    /**
     * Set Credentials for Request and update authorization params.
     *
     * @param Credentials $credentials
     */
    public function setCredentials(Credentials $credentials)
    {
        $this->credentials = $credentials;
        $this->inflateAuthParams();
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    public function setUrl(Url $url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @param string $httpMethod
     */
    public function setHttpMethod($httpMethod)
    {
        $this->httpMethod = $httpMethod;
    }

}
