<?php

namespace App\Datasource\Twitter;

use App\Datasource\OAuth\Request;
use App\Datasource\OAuth\ValueObject\Credentials;
use App\Datasource\OAuth\ValueObject\Url;
use App\Datasource\Twitter\Exception\UserNotFoundException;

class Client
{
    /**
     * Credentials for OAuth authenticated API calls.
     *
     * @var Credentials
     */
    protected $credentials;

    /**
     * Credentials for OAuth authenticated API calls.
     *
     * @var Credentials
     */
    protected $request;

    public function __construct(Credentials $credentials, Request $request = null)
    {
        $this->credentials = $credentials;
        $this->request = is_null($request) ? new Request($this->credentials) : $request;
    }

    /**
     * Get latest tweets for given user.
     *
     * Params should contain for example:
     * [
     *   'screen_name' => 'nasa',
     *   'count' => '7'
     * ]
     * where screen_name is required and count is optional.
     *
     * @param array $params Parameters for search query (i.e. user screen name, and tweets count)
     * @return array
     * @throws \App\Datasource\OAuth\Exception\InvalidUrlException
     * @throws Exception\InvalidDtoException
     * @throws UserNotFoundException
     */
    public function getUserTimelineStatuses($params)
    {
        $url = new Url('api.twitter.com', '/1.1/statuses/user_timeline.json');
        $this->request->setUrl($url);
        $result = $this->request->execute($params);
        if (!is_array($result)) {
            if (isset($result->errors)) {
                foreach ($result->errors as $error) {
                    // TODO handle more error codes
                    if ($error->code = 34) {
                        throw new UserNotFoundException($error->message);
                    }
                }
            }
        }
        $tweets = [];
        $tweetFactory = new TweetFactory();
        foreach ($result as $tweetDto) {
            $tweets[] = $tweetFactory->createTweet($tweetDto);
        }
        return $tweets;
    }

}
