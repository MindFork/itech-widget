<?php

namespace App\Datasource\Twitter\Exception;

class UserNotFoundException extends \Exception {}
