<?php

namespace App\Datasource\Twitter;

use App\Datasource\Twitter\Exception\InvalidDtoException;
use App\Datasource\Twitter\ValueObject\Media;
use App\Datasource\Twitter\ValueObject\Tweet;
use App\Datasource\Twitter\ValueObject\Url;
use App\Datasource\Twitter\ValueObject\User;

class TweetFactory
{
    /**
     * @param $tweetDto
     * @return Tweet
     * @throws InvalidDtoException
     */
    public function createTweet($tweetDto)
    {
        $this->validateTweetDto($tweetDto);
        return new Tweet(
            $tweetDto->id,
            $tweetDto->text,
            new \DateTime($tweetDto->created_at),
            $this->extractHashtags($tweetDto),
            $this->extractUserMentions($tweetDto),
            $this->extractUrls($tweetDto),
            $this->extractMedia($tweetDto),
            $this->extractUser($tweetDto)
        );
    }

    /**
     * Verify if provided object is proper tweet representation.
     *
     * @param $tweetDto
     * @throws InvalidDtoException
     */
    private function validateTweetDto($tweetDto)
    {
        if (!is_object($tweetDto)) {
            throw new InvalidDtoException('Invalid DTO object');
        }
        $dtoVars = array_keys(get_object_vars($tweetDto));
        $expectedVars = [
            'created_at',
            'id',
            'text',
            'entities',
            'user'
        ];
        foreach ($expectedVars as $expectedVar) {
            if (!in_array($expectedVar, $dtoVars)) {
                throw new InvalidDtoException('Invalid tweet data, missing \'' . $expectedVar . '\' parameter');
            }
        }
    }

    /**
     * @param $tweetDto
     * @return User
     */
    private function extractUser($tweetDto)
    {
        return new User(
            $tweetDto->user->screen_name,
            $tweetDto->user->name,
            $tweetDto->user->id,
            $tweetDto->user->description,
            $tweetDto->user->statuses_count,
            $tweetDto->user->followers_count,
            $tweetDto->user->profile_image_url
        );
    }

    /**
     * @param $tweetDto
     * @return string[]
     */
    private function extractHashtags($tweetDto)
    {
        $hashtags = [];
        foreach ($tweetDto->entities->hashtags as $hashtag) {
            $hashtags[] = $hashtag->text;
        }
        return $hashtags;
    }

    /**
     * @param $tweetDto
     * @return User[]
     */
    private function extractUserMentions($tweetDto)
    {
        $users = [];
        foreach ($tweetDto->entities->user_mentions as $userMention) {
            $users[] = new User(
                $userMention->screen_name,
                $userMention->name,
                $userMention->id
            );
        }
        return $users;
    }

    /**
     * @param $tweetDto
     * @return Url[]
     */
    private function extractUrls($tweetDto)
    {
        $urls = [];
        foreach ($tweetDto->entities->urls as $url) {
            $urls[] = new Url(
                $url->url,
                $url->expanded_url,
                $url->display_url
            );
        }
        return $urls;
    }

    /**
     * @param $tweetDto
     * @return Media[]
     */
    private function extractMedia($tweetDto)
    {
        $media = [];
        if (!empty($tweetDto->entities->media)) {
            foreach ($tweetDto->entities->media as $mediaItem) {
                $media[] = new Media(
                    $mediaItem->url,
                    $mediaItem->expanded_url,
                    $mediaItem->display_url,
                    $mediaItem->media_url,
                    $mediaItem->type
                );
            }
        }
        return $media;
    }

}
