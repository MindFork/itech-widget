<?php

namespace App\Datasource\Twitter\ValueObject;

final class Media
{
    /**
     * Short URL.
     *
     * @var string
     */
    private $url;

    /**
     * "Regular" URL.
     * @var string
     */
    private $expandedUrl;

    /**
     * Link text.
     *
     * @var string
     */
    private $displayUrl;

    /**
     * Media URL.
     *
     * @var string
     */
    private $mediaUrl;

    /**
     * Type of media.
     *
     * @var string
     */
    private $type;

    /**
     * Url constructor.
     *
     * @param string $url
     * @param string $expandedUrl
     * @param string $displayUrl
     * @param string $mediaUrl
     * @param string $type
     */
    public function __construct($url, $expandedUrl, $displayUrl, $mediaUrl, $type)
    {
        $this->url = $url;
        $this->expandedUrl = $expandedUrl;
        $this->displayUrl = $displayUrl;
        $this->mediaUrl = $mediaUrl;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getExpandedUrl()
    {
        return $this->expandedUrl;
    }

    /**
     * @return string
     */
    public function getDisplayUrl()
    {
        return $this->displayUrl;
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->mediaUrl;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
