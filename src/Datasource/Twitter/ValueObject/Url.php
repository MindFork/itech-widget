<?php

namespace App\Datasource\Twitter\ValueObject;

final class Url
{
    /**
     * Short URL.
     *
     * @var string
     */
    private $url;

    /**
     * "Regular" URL.
     * @var string
     */
    private $expandedUrl;

    /**
     * Link text.
     *
     * @var string
     */
    private $displayUrl;

    /**
     * Url constructor.
     *
     * @param string $url
     * @param string $expandedUrl
     * @param string $displayUrl
     */
    public function __construct($url, $expandedUrl, $displayUrl)
    {
        $this->url = $url;
        $this->expandedUrl = $expandedUrl;
        $this->displayUrl = $displayUrl;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getExpandedUrl()
    {
        return $this->expandedUrl;
    }

    /**
     * @return string
     */
    public function getDisplayUrl()
    {
        return $this->displayUrl;
    }
}
