<?php

namespace App\Datasource\Twitter\ValueObject;

/**
 * Class User.
 *
 * Represents a Twitter user.
 *
 * @package App\Datasource\Twitter\ValueObject
 */
final class User
{
    /**
     * Name suitable for mentions and queries.
     *
     * @var string
     */
    private $screenName;

    /**
     * Full user name.
     *
     * @var string
     */
    private $name;

    /**
     * Twitter ID.
     *
     * @var integer
     */
    private $id;

    /**
     * Description.
     *
     * @var string
     */
    private $description;

    /**
     * Number of tweets.
     *
     * @var integer
     */
    private $statusesCount;

    /**
     * Number of followers.
     *
     * @var integer
     */
    private $followersCount;

    /**
     * Profile image URL.
     *
     * @var string
     */
    private $profileImageUrl;

    /**
     * User constructor.
     *
     * @param string $screenName
     * @param string $name
     * @param int $id
     * @param string $description
     * @param int $statusesCount
     * @param int $followersCount
     * @param string $profileImageUrl
     */
    public function __construct(
        $screenName,
        $name,
        $id,
        $description = null,
        $statusesCount = null,
        $followersCount = null,
        $profileImageUrl= null
    )
    {
        $this->screenName = $screenName;
        $this->name = $name;
        $this->id = $id;
        $this->description = $description;
        $this->statusesCount = $statusesCount;
        $this->followersCount = $followersCount;
        $this->profileImageUrl = $profileImageUrl;
    }

    /**
     * @return string
     */
    public function getScreenName()
    {
        return $this->screenName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getStatusesCount()
    {
        return $this->statusesCount;
    }

    /**
     * @return int
     */
    public function getFollowersCount()
    {
        return $this->followersCount;
    }

    /**
     * @return string
     */
    public function getProfileImageUrl()
    {
        return $this->profileImageUrl;
    }
}
