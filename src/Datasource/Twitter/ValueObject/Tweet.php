<?php

namespace App\Datasource\Twitter\ValueObject;

final class Tweet
{
    /**
     * Tweet ID.
     *
     * @var integer
     */
    private $id;

    /**
     * Tweet content.
     *
     * @var string
     */
    private $text;

    /**
     * Tweet creation date and time.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * List of hashtags.
     *
     * @var string[]
     */
    private $hastags;

    /**
     * List of user mentions.
     *
     * @var User[]
     */
    private $userMentions;

    /**
     * List of URLs.
     *
     * @var Url[]
     */
    private $urls;

    /**
     * List of URLs.
     *
     * @var Media[]
     */
    private $media;

    /**
     * Author of the Tweet.
     *
     * @var User
     */
    private $user;

    /**
     * Tweet constructor.
     *
     * @param int $id
     * @param string $text
     * @param \DateTime $createdAt
     * @param array $hastags
     * @param array $userMentions
     * @param array $urls
     * @param array $media
     * @param User $user
     */
    public function __construct(
        $id,
        $text,
        \DateTime $createdAt,
        array $hastags = [],
        array $userMentions = [],
        array $urls = [],
        array $media = [],
        User $user
    )
    {
        $this->id = $id;
        $this->text = $text;
        $this->createdAt = $createdAt;
        $this->hastags = $hastags;
        $this->userMentions = $userMentions;
        $this->urls = $urls;
        $this->media = $media;
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string[]
     */
    public function getHastags()
    {
        return $this->hastags;
    }

    /**
     * @return User[]
     */
    public function getUserMentions()
    {
        return $this->userMentions;
    }

    /**
     * @return Url[]
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Media[]
     */
    public function getMedia()
    {
        return $this->media;
    }
}
