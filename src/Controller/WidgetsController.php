<?php

namespace App\Controller;

use App\Datasource\OAuth\Exception\InvalidCredentialsException;
use App\Datasource\OAuth\Exception\InvalidUrlException;
use App\Datasource\OAuth\ValueObject\Credentials;
use App\Datasource\Twitter\Client;
use App\Datasource\Twitter\Exception\InvalidDtoException;
use App\Datasource\Twitter\Exception\UserNotFoundException;
use Cake\Cache\Cache;

class WidgetsController extends AppController
{
    /**
     * Fetch latest tweets for given user.
     *
     * @param string $user Twitter screen name
     */
    public function twitter($user)
    {
        try {
            $credentials = new Credentials(
                '6LNH1mvehWvBIu9sUbyqm3N88',
                's46TbZGXv3rJOWWesb48vsFF1Spk9jGGSQuerTWg0kHNQDfTVC',
                '169836904-llxVS74VdGQO8h7IUdLo6apld6yeOJbsX9VTR9Z7',
                'sUqpXGP4d6SJoJDVwDiCrIxcDjLiC9KF6PZA6bHk2tEgq'
            );
            $twitterClient = new Client($credentials);
            if (($latestTweets = Cache::read($user . '_tweets')) === false) {
                $latestTweets = $twitterClient->getUserTimelineStatuses([
                    'screen_name' => $user,
                    'count' => 8,
                ]);
                Cache::write($user . '_tweets', $latestTweets, 'twitter');
            }
            $this->set('latestTweets', [
                'success' => true,
                'tweets' => $latestTweets
            ]);
        } catch (InvalidCredentialsException $e) {
            $this->set('latestTweets', [
                'success' => false,
                'error' => 'Malformed data: ' . $e->getMessage()
            ]);
        } catch (InvalidUrlException $e) {
            $this->set('latestTweets', [
                'success' => false,
                'error' => 'Invalid API URL: ' . $e->getMessage()
            ]);
        } catch (InvalidDtoException $e) {
            $this->set('latestTweets', [
                'success' => false,
                'error' => 'Malformed data: ' . $e->getMessage()
            ]);
        } catch (UserNotFoundException $e) {
            $this->set('latestTweets', [
                'success' => false,
                'error' => 'User does not exist!'
            ]);
        }
    }
}
