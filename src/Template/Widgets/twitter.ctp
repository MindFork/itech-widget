<?php

/** @var array $latestTweets */
/** @var \App\Datasource\Twitter\ValueObject\Tweet $tweet */
?>
<div class="retwit__widget">
    <?php if ($latestTweets['success']): ?>
    <?php foreach ($latestTweets['tweets'] as $tweet): ?>
        <div class="tweet">
            <div class="user">
                <div class="user__image">
                    <img src="<?php echo $tweet->getUser()->getProfileImageUrl() ?>" />
                </div>
                <div class="user__name">
                    <h2><?php echo $tweet->getUser()->getName() ?></h2>
                    <span>
                        <a href="https://twitter.com/<?php echo $tweet->getUser()->getScreenName()?>" target="_blank">
                            @<?php echo $tweet->getUser()->getScreenName()?>
                        </a>
                    </span>
                </div>
            </div>
            <div class="tweet__text">
                <?php
                    $text = str_ireplace('RT ', '<i class="fas fa-retweet">&nbsp;</i>', $tweet->getText());
                    foreach ($tweet->getUserMentions() as $userMention) {
                        $annotation = '@'.$userMention->getScreenName();
                        $link = '<a href="https://twitter.com/'. $userMention->getScreenName() . '" target="_blank">' . $annotation . '</a>';
                        $text = str_ireplace($annotation, $link, $text);
                    }
                    foreach ($tweet->getHastags() as $hastagName) {
                        $hashtag = '#'.$hastagName;
                        $link = '<a href="https://twitter.com/hashtag/'. $hastagName . '" target="_blank">' . $hashtag . '</a>';
                        $text = str_ireplace($hashtag, $link, $text);
                    }
                    foreach ($tweet->getUrls() as $url) {
                        $urlText = $url->getDisplayUrl();
                        $link = '<a href="' . $url->getUrl() . '" target="_blank">' . $urlText . '</a>';
                        $text = str_ireplace($url->getUrl(), $link, $text);
                        $text = str_ireplace($urlText, $link, $text);
                    }
                    foreach ($tweet->getMedia() as $media) {
                        $mediaDisplayUrl = $media->getDisplayUrl();
                        $link = '<a href="' . $media->getUrl() . '" target="_blank">' . $mediaDisplayUrl . '</a>';
                        $text = str_ireplace($media->getUrl(), $link, $text);
                        $text = str_ireplace($mediaDisplayUrl, $link, $text);
                    }
                    echo $text;
                ?>
            </div>
        </div>
    <?php endforeach; ?>
    <?php else: ?>
        <h1><?=$latestTweets['error']?></h1>
    <?php endif; ?>
</div>

